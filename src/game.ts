@Component('wheelSpin')
export class WheelSpin {
  active: boolean = false
  speed: number = 30
  direction: Vector3 = Vector3.Up()
}

const wheels = engine.getComponentGroup(WheelSpin)

// This system carries out the rotation on each frame
export class RotatorSystem implements ISystem {
 
  update(dt: number) {
    // iterate over the wheels in the component group
    for (let wheel of wheels.entities) {
      // handy shortcuts
      let spin = wheel.getComponent(WheelSpin)
      let transform = wheel.getComponent(Transform)
      // check state
      if (spin.active){
        // spin the wheel
        transform.rotate(spin.direction, spin.speed * dt)
      }
    }
  }
}

engine.addSystem(new RotatorSystem())

// Create wheel entities
let wheel1 = new Entity()
wheel1.addComponent(new CylinderShape())
wheel1.addComponent(new Transform({
  position: new Vector3(6, 2, 11.9),
  rotation: Quaternion.Euler(90, 0, 0),
  scale: new Vector3(1, 0.05, 1)
}))
engine.addEntity(wheel1)

let wheel2 = new Entity()
wheel2.addComponent(new CylinderShape())
wheel2.addComponent(new Transform({
  position: new Vector3(10, 2, 11.9),
  rotation: Quaternion.Euler(90, 0, 0),
  scale: new Vector3(1, 0.05, 1)
}))
engine.addEntity(wheel2)

// Create texture
const spiralTexture = new Texture("materials/hypno-wheel.png")

// Create material
let spiralMaterial = new Material()
spiralMaterial.albedoTexture = spiralTexture

// Add material to wheels
wheel1.addComponent(spiralMaterial)
wheel2.addComponent(spiralMaterial)

// Add the custom component to the wheels
wheel1.addComponent(new WheelSpin())
wheel2.addComponent(new WheelSpin())

// Change the direction for wheel2 (wheel1 is left with the default direction `Up`)
wheel2.getComponent(WheelSpin).direction = Vector3.Down()

// Set the click behavior for the wheels
wheel1.addComponent(
  new OnClick(e => {
    let spin = wheel1.getComponent(WheelSpin)
    if (!spin.active){
      spin.active = true
    } else {
      spin.speed += 20
    }
    //log("speed: ", spin.speed)
  })
)

wheel2.addComponent(
  new OnClick(e => {
    let spin = wheel2.getComponent(WheelSpin)
    if (!spin.active){
      spin.active = true
    } else {
      spin.speed += 30
    }
    //log("speed: ", spin.speed)
  })
)

//This is needed for the animation, but using it does not work, weirdly. Not sure what's going on here!
import utils from "../node_modules/decentraland-ecs-utils/index"

const Egg = new Entity()

Egg.addComponent(new Transform())

//Egg.getComponent(Transform).position.set(1, 1, 1)

Egg.getComponent(Transform).scale.set(1, 1, 1)

engine.addEntity(Egg)

Egg.addComponent(new GLTFShape("models/Egg.glb"))

Egg.addComponent(new OnClick(e => {
    let StartPos = new Vector3(1, 1, 1)
    let EndPos = new Vector3(15, 1, 15)

    Egg.addComponent (new utils.MoveTransformComponent(StartPos, EndPos, 2));
  }))
